<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    use HasFactory;

    protected $fillable = [
        'cedula', 'nombres','apellidos','email','pais','direccion','celular','categoria_id'
    ];

    public function categoria()
    {
        return $this->hasMany('App\Models\Categoria','id','categoria_id');
    }


}
