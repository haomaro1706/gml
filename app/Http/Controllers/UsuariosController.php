<?php

namespace App\Http\Controllers;

use App\Mail\NotificacionAdmin;
use App\Mail\NotificacionUsuario;
use App\Models\Categoria;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $usuarios = Usuario::all();
        return view('usuarios')
            ->with('usuarios', $usuarios);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $paises = ($this->flitrarPaises());
        $categorias = Categoria::all();

        return view('usuarios_crear')
        ->with('paises', $paises)
        ->with('categorias', $categorias);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $this->validate($request,[
            'cedula'=>'required|unique:usuarios,cedula',
            'nombres'=>'required|max:100|min:5',
            'apellidos'=>'required|max:100|min:5',
            'email'=>'required|max:150|email',
            'direccion'=>'required|max:180',
            'celular'=>'required|numeric',
        ]);
        //dd($data);
        try {

            DB::beginTransaction();
            $input = [
            'cedula' => $data['cedula'],
            'nombres' => $data['nombres'],
            'apellidos' => $data['apellidos'],
            'email' => $data['email'],
            'pais' => $data['pais'],
            'direccion' => $data['direccion'],
            'celular' => $data['celular'],
            'categoria_id' => $data['categoria'],
            ];
            $usuario = Usuario::create($input);

            Mail::to($usuario->email)->send(new NotificacionUsuario($usuario));
            $this->notificacionAdmin();

            DB::commit();

            return redirect(route('usuarios.index'))
                ->with('success', 'Usuario creado exitosamente.');


        }catch(\Exception $e){
            //dd($e->getMessage());
            return redirect(route('usuarios.index'))
                ->with('error', 'Error creando usuario.'.$e->getMessage());
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $usuario = Usuario::find($id);
        $paises = ($this->flitrarPaises());
        $categorias = Categoria::all();

        return view('usuarios_editar')
        ->with('paises', $paises)
        ->with('categorias', $categorias)
        ->with('usuario', $usuario);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $data = $request->all();
        //dd($data);
        try {

            DB::beginTransaction();

            Usuario::find($id)->update([
                'cedula' => $data['cedula'],
                'nombres' => $data['nombres'],
                'apellidos' => $data['apellidos'],
                'email' => $data['email'],
                'pais' => $data['pais'],
                'direccion' => $data['direccion'],
                'celular' => $data['celular'],
                'categoria_id' => $data['categoria']
            ]);

            DB::commit();

            return redirect(route('usuarios.index'))
                ->with('success', 'Usuario editado exitosamente.');


        }catch(\Exception $e){
            //dd($e->getMessage());
            return redirect(route('usuarios.index'))
                ->with('error', 'Error editando usuario.'.$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {

        try {

            DB::beginTransaction();

            Usuario::find($id)->delete();

            DB::commit();

            return redirect(route('usuarios.index'))
                ->with('success', 'Usuario borrado exitosamente.');


        }catch(\Exception $e){
            //dd($e->getMessage());
            return redirect(route('usuarios.index'))
                ->with('error', 'Error borradoo usuario.'.$e->getMessage());
        }
    }

    private function getPaises()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://restcountries.com/v3.1/region/South%20America',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }

    private function flitrarPaises()
    {
        $paises = json_decode($this->getPaises(),true);
        $data = [];
        foreach($paises As $pais)
        {
            $data[$pais['name']['common']] = $pais['name']['common'];
        }
        return $data;
    }

    private function notificacionAdmin()
    {
        $correoAdmin = env('ADMIN_CORREO');
        Mail::to($correoAdmin)->send(new NotificacionAdmin());
    }
}
