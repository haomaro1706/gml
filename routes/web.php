<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('usuarios', [App\Http\Controllers\UsuariosController::class, 'index'])->name('usuarios');
Route::get('usuarios/crear', [App\Http\Controllers\UsuariosController::class, 'create'])->name('usuarios.crear');
Route::post('usuarios.store', [App\Http\Controllers\UsuariosController::class, 'store'])->name('usuarios.store');*/

Route::resource('usuarios', App\Http\Controllers\UsuariosController::class);
