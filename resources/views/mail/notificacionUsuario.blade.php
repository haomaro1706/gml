<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Usuario registrado</title>
</head>
<body>
    <p>Hola! {!! $usuario->nombres !!}.</p>
    <p>ya estas  registrado con los siguientes datos</p>
    <ul>
        <li>Nombre: {{ $usuario->nombres }}</li>
        <li>Apellidos: {{ $usuario->apellidos }}</li>
        <li>Cedula: {{ $usuario->cedula }}</li>
        <li>Pais: {{ $usuario->pais }}</li>
        <li>Dirección: {{ $usuario->direccion }}</li>
        <li>Celular: {{ $usuario->celular }}</li>
    </ul>
</body>
</html>
