<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Reporte Usuarios</title>
</head>
<body>
    <p>Hola se creo un nuevo registro este es el listado actualizado.</p>

    <table>
        <tr>
            <td>Nombre</td>
            <td>Apellidos</td>
            <td>Cedula</td>
            <td>Pais</td>
            <td>Dirección</td>
            <td>Celular</td>
        </tr>
        @foreach ($usuarios as $usuario)
            <tr>
            <td> {!! $usuario->nombres !!}</td>
            <td> {!! $usuario->apellidos !!}</td>
            <td> {!! $usuario->cedula !!}</td>
            <td> {!! $usuario->pais !!}</td>
            <td> {!! $usuario->direccion !!}</td>
            <td> {!! $usuario->celular !!}</td>
            </tr>
        @endforeach
    </table>
</body>
</html>
