@extends('app')

@section('titulo','Usuarios')

@section('contenido')

@if ($errors->any())
    <div class="col-sm-12">
        <div class="alert  alert-warning alert-dismissible fade show" role="alert">
            @foreach ($errors->all() as $error)
                <span><p>{{ $error }}</p></span>
            @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    </div>
@endif

@if (session('success'))
    <div class="col-sm-12">
        <div class="alert  alert-success alert-dismissible fade show" role="alert">
            {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    </div>
@endif

@if (session('error'))
    <div class="col-sm-12">
        <div class="alert  alert-danger alert-dismissible fade show" role="alert">
            {{ session('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    </div>
@endif


<a class="btn btn-primary" href="{{ route('usuarios.create') }}">Crear usuario</a>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Cedula</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Email</th>
            <th>Celular</th>
            <th>Categoria</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($usuarios as $usuario)
            <tr>
            <td>{!! $usuario->cedula !!}</td>
            <td>{!! $usuario->nombres !!}</td>
            <td>{!! $usuario->apellidos !!}</td>
            <td>{!! $usuario->email !!}</td>
            <td>{!! $usuario->celular !!}</td>
            <td>{!! $usuario->categoria()->first()->categoria !!}</td>
            <td>
                <a class="btn btn-success" href="{!! route('usuarios.edit',[$usuario->id]) !!}">Editar</a>
                <form action="{{ route('usuarios.destroy', $usuario->id) }}" style="display:inline" method="POST" onsubmit="return confirm('¿Está seguro?');">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Borrar">Borrar</button>
                </form>
            </td>
            </tr>
        @endforeach
    </tbody>
</table>

@endsection

