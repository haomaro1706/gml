@extends('app')

@section('titulo','Editar usuarios')

@section('contenido')

<form method="POST" action="{!! route('usuarios.update',$usuario->id) !!}">
    @csrf

    @method('PUT')

    <div class="mb-3">
      <label for="cedula" class="form-label">Cedula</label>
      <input type="number" class="form-control" id="cedula" name="cedula" value="{!! $usuario->cedula !!}">
    </div>
    <div class="mb-3">
        <label for="nombres" class="form-label">Nombres</label>
        <input type="text" class="form-control" id="nombres" name="nombres" value="{!! $usuario->nombres !!}">
    </div>
    <div class="mb-3">
        <label for="apellidos" class="form-label">Apellidos</label>
        <input type="text" class="form-control" id="apellidos" name="apellidos" value="{!! $usuario->apellidos !!}">
    </div>
    <div class="mb-3">
        <label for="email" class="form-label">Email</label>
        <input type="email" class="form-control" id="email" name="email" value="{!! $usuario->email !!}">
    </div>
    <div class="mb-3">
        <label for="pais" class="form-label">Pais</label>
        <select id="pais" name="pais" class="form-select">
            <option>seleccionar</option>
            @foreach ($paises as $key=>$pais)
                @php
                $select= '';
                if($pais == $usuario->pais){
                    $select = 'selected';
                }
                @endphp
                <option {!! $select !!} value="{!! $key !!}">{!! $pais !!}</option>
            @endforeach
        </select>
    </div>
    <div class="mb-3">
        <label for="direccion" class="form-label">Dirección</label>
        <input type="text" class="form-control" id="direccion" name="direccion" value="{!! $usuario->direccion !!}">
    </div>
    <div class="mb-3">
        <label for="celular" class="form-label">Celular</label>
        <input type="number" class="form-control" id="celular" name="celular" value="{!! $usuario->celular !!}">
    </div>
    <div class="mb-3">
        <label for="categoria" class="form-label">Categoria</label>
        <select id="categoria" name="categoria" class="form-select">
            <option>seleccionar</option>
            @foreach ($categorias as $categoria)
                @php
                $select= '';
                if($categoria->id == $usuario->categoria_id){
                    $select = 'selected';
                }
                @endphp
                <option {!! $select !!} value="{!! $categoria->id !!}">{!! $categoria->categoria !!}</option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Editar</button>
  </form>

@endsection

