@extends('app')

@section('titulo','Usuarios')

@section('contenido')

@if ($errors->any())
    <div class="col-sm-12">
        <div class="alert  alert-warning alert-dismissible fade show" role="alert">
            @foreach ($errors->all() as $error)
                <span><p>{{ $error }}</p></span>
            @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    </div>
@endif

@if (session('success'))
    <div class="col-sm-12">
        <div class="alert  alert-success alert-dismissible fade show" role="alert">
            {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    </div>
@endif

@if (session('error'))
    <div class="col-sm-12">
        <div class="alert  alert-danger alert-dismissible fade show" role="alert">
            {{ session('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    </div>
@endif

<form method="POST" action="{{ route('usuarios.store') }}">
    {{ csrf_field() }}
    <div class="mb-3">
      <label for="cedula" class="form-label">Cedula</label>
      <input type="number" class="form-control" id="cedula" name="cedula" >
    </div>
    <div class="mb-3">
        <label for="nombres" class="form-label">Nombres</label>
        <input type="text" class="form-control" id="nombres" name="nombres">
    </div>
    <div class="mb-3">
        <label for="apellidos" class="form-label">Apellidos</label>
        <input type="text" class="form-control" id="apellidos" name="apellidos">
    </div>
    <div class="mb-3">
        <label for="email" class="form-label">Email</label>
        <input type="email" class="form-control" id="email" name="email">
    </div>
    <div class="mb-3">
        <label for="pais" class="form-label">Pais</label>
        <select id="pais" name="pais" class="form-select">
            <option>seleccionar</option>
            @foreach ($paises as $key=>$pais)
            <option value="{!! $key !!}">{!! $pais !!}</option>
            @endforeach
        </select>
    </div>
    <div class="mb-3">
        <label for="direccion" class="form-label">Dirección</label>
        <input type="text" class="form-control" id="direccion" name="direccion">
    </div>
    <div class="mb-3">
        <label for="celular" class="form-label">Celular</label>
        <input type="number" class="form-control" id="celular" name="celular">
    </div>
    <div class="mb-3">
        <label for="categoria" class="form-label">Categoria</label>
        <select id="categoria" name="categoria" class="form-select">
            <option>seleccionar</option>
            @foreach ($categorias as $categoria)
            <option value="{!! $categoria->id !!}">{!! $categoria->categoria !!}</option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Crear</button>
  </form>

@endsection

