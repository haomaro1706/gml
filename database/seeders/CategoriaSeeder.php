<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $categorias = array(
            array('categoria' => 'Cliente ', 'created_at' => NULL,'updated_at' => NULL),
            array('categoria' => 'Proveedor', 'created_at' => NULL,'updated_at' => NULL),
            array('categoria' => 'Funcionario interno', 'created_at' => NULL,'updated_at' => NULL),
        );

        DB::table('categorias')->insert($categorias);

    }
}
